import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getFormDesc() {
    return element(by.css('app-root .weather-choose-location__description')).getText();
  }
}
