export const environment = {
  production: true,
  socketServer: 'http://test.itksb.com:8080/ws',
  apiUrl: 'http://test.itksb.com:8080',
  debug: false
};
