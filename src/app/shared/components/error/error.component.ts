import { Component, OnInit, Input } from '@angular/core';

/**
 * Displaying errors to the user
 */
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  @Input() errorInput: string;
  error: string;
  errorMessage = {
    lostConnection: 'Произошла ошибка соединения. Пожалуйста, попробуйте ещё раз.',
    default: 'Произошла ошибка. Пожалуйста, попробуйте ещё раз.',
    httpFail: 'Произошла ошибка получения данных. Пожалуйста, попробуйте позже.'
  };

  /**
   * @ignore
   */
  constructor() { }

  /**
   * @ignore
   */
  ngOnInit() {
  }

  ngOnChanges() {
    if (this.errorInput.toLowerCase().indexOf('lost connection') !== -1) {
      this.error = this.errorMessage['lostConnection'];
    } else if (this.errorInput.toLowerCase().indexOf('http failure') !== -1) {
      this.error = this.errorMessage['httpFail'];
    } else if (this.errorInput) {
      console.error(this.errorInput);
      this.error = this.errorInput;
    } else {
      console.error(this.errorInput);
      this.error = this.errorMessage.default;
    }
  }
}
