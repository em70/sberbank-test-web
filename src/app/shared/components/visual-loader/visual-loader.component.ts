import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visual-loader',
  templateUrl: './visual-loader.component.html',
  styleUrls: ['./visual-loader.component.css']
})
export class VisualLoaderComponent implements OnInit {
  /**
   * @ignore
   */
  constructor() { }

  /**
   * @ignore
   */
  ngOnInit() {
  }

}
