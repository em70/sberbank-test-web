import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { VisualLoaderComponent } from './components/visual-loader/visual-loader.component';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
  declarations: [ VisualLoaderComponent, ErrorComponent ],
  exports: [
    FormsModule, ReactiveFormsModule, NgSelectModule,
    RouterModule, CommonModule, HttpClientModule,
    VisualLoaderComponent, ErrorComponent
  ]
})
export class SharedModule { }
