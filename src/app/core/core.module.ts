import { NgModule, Optional, SkipSelf } from '@angular/core';

import { environment } from '../../environments/environment';

import { LocationAbstract } from './services/location/location.abstract';
import { LocationService } from './services/location/location.service';

import { throwIfAlreadyLoaded } from './module-import-guard';

@NgModule({
  providers: [
    {
      provide: LocationAbstract,
      useClass: LocationService
    }
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule ) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
