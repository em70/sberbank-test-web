import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { of, from } from 'rxjs';
import { map } from 'rxjs/operators/map';

import { environment } from '../../../../environments/environment';

import { ILocation } from './location';
import { LocationAbstract } from './location.abstract';

/**
 * The service provides the processing of the location selected by the user,
 * requests the coordinates of the selected location from the server.
 */
@Injectable({
  providedIn: 'root'
})
export class LocationService extends LocationAbstract {

  constructor(private http: HttpClient) {
    super();
    this.locations = [];
  }

  /**
   * Requests to the server for the location coordinates
   * @param {string} city
   * @returns {Observable<ILocation>}
   */
  getLocation(city: string): Observable<ILocation> {
    if (this.locations.length > 0) {
      return of(this.locations.filter(location => location.city === city))
        .pipe(map(locations => {
          return locations[0];
        }));
    }

    return this.getLocations()
      .pipe(
        map(locations => {
          const found = locations.filter(location => location.city === city);
          return found[0];
        })
      );
  }

  /**
   * Requests to the server for the available locations
   * @returns {Observable<ILocation[]>}
   */
  getLocations(): Observable<ILocation[]> {
    if (this.locations.length > 0) {
      return of(this.locations);
    }

    return this.http.get<ILocation[]>(`${environment.apiUrl}/geo-locations`)
      .pipe(map(locations => {
        this.locations = locations;
        return this.locations;
      }));
  }
}
