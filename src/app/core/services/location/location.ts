export interface ILocation {
  city: string;
  longitude: number;
  latitude: number;
  id: number;
  region: string;
}
