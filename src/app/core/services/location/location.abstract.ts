import { Observable } from 'rxjs';
import { ILocation } from './location';

/**
 * Processing of the location selected by the user
 */
export abstract class LocationAbstract {
  /**
   * Locations cache
   */
  locations: any[];

  /**
   * Requests to the server for the location coordinates
   * @param {string} city
   * @returns {Observable<ILocation>}
   */
  abstract getLocation(city: string): Observable<ILocation>;

  /**
   * Requests to the server for the available locations
   * @returns {Observable<any>}
   */
  abstract getLocations(): Observable<ILocation[]>;
}
