/**
 * Provides user authentication
 */
export abstract class AuthenticationAbstract {
  /**
   * User authentication
   * @returns {boolean}
   */
  abstract authenticate(...args: any[]): boolean;

  /**
   * Check user authentication
   * @returns {boolean}
   */
  abstract isAuthenticated(): boolean;
}
