/**
 * Global storage for user data
 */
export abstract class StorageAbstract {
  /**
   * Takes data from the storage
   * @param {string} key
   * @returns {string}
   */
  abstract get(key: string): string;

  /**
   * Putting data to the storage
   * @param {string} key
   * @param {string} value
   */
  abstract set(key: string, value: string): void;

  /**
   * Deleting data from the storage
   * @param {string} key
   */
  abstract delete(key: string): void;
}
