import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

/**
 * Visually represents loading of the current weather
 */
@Component({
  selector: 'app-weather-current-loader',
  templateUrl: './weather-current-loader.component.html',
  styleUrls: ['./weather-current-loader.component.css']
})
export class WeatherCurrentLoaderComponent implements OnInit {
  /**
   * User position
   */
  @Input() position: number;

  /**
   * @ignore
   */
  constructor() { }

  /**
   * @ignore
   */
  ngOnInit() {
  }
}
