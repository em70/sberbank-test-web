import { NgModule } from '@angular/core';
import { NG_SELECT_DEFAULT_CONFIG } from '@ng-select/ng-select';

import { SharedModule } from '../shared/shared.module';
import { WeatherComponent } from './weather.component';
import { WeatherChooseLocationComponent } from './components/weather-choose-location/weather-choose-location.component';
import { WeatherCurrentComponent } from './components/weather-current/weather-current.component';
import { WeatherItemComponent } from './components/weather-item/weather-item.component';
import { WeatherCurrentLoaderComponent } from './components/weather-current-loader/weather-current-loader.component';
import { AutofocusDirective } from '../shared/directives/autofocus.directive';

import { CurrentWeatherService } from './services/current-weather/current-weather.service';
/*import { MockedCurrentWeatherService as CurrentWeatherService } from './services/current-weather/mocked-current-weather.service';*/
import { CurrentWeatherAbstract } from './services/current-weather/current-weather.abstract';

import { WeatherTransportService } from './services/weather-transport/weather-transport.service';
import { WeatherTransportAbstract } from './services/weather-transport/weather-transport.abstract';
import { WeatherTransportConfigAbstract } from './services/weather-transport/weather-transport-config.abstract';
import { weatherTransportConfig } from './weather-transport-config';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    WeatherComponent,
    WeatherChooseLocationComponent,
    WeatherCurrentComponent,
    WeatherItemComponent,
    WeatherCurrentLoaderComponent,
    AutofocusDirective
  ],
  providers: [
    {
      provide: CurrentWeatherAbstract,
      useClass: CurrentWeatherService
    },
    {
      provide: WeatherTransportAbstract,
      useClass: WeatherTransportService
    },
    {
      provide: WeatherTransportConfigAbstract,
      useValue: weatherTransportConfig
    },
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
        notFoundText: 'Город не найден',
        loadingText: 'Загрузка...'
      }
    }
  ]
})
export class WeatherModule { }
