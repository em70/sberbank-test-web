import * as SockJS from 'sockjs-client';

import { WeatherTransportConfigAbstract } from './services/weather-transport/weather-transport-config.abstract';
import { environment } from '../../environments/environment';

/**
 * Socket provider for the WeatherTransportService
 */
function socketProvider() {
  return new SockJS(environment.socketServer);
}

export const weatherTransportConfig: WeatherTransportConfigAbstract = {
  url: socketProvider,
  headers: {},
  heartbeat_in: 0,
  heartbeat_out: 20000,
  reconnect_delay: 5000,
  debug: environment.debug
};
