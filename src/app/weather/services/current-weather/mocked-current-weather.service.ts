import { Injectable } from '@angular/core';
import { from } from 'rxjs/observable/from';
import { mergeAll } from 'rxjs/operators';

import { ILocation } from '../../../core/services/location/location';
import { CurrentWeatherAbstract } from './current-weather.abstract';

/**
 * The mocked CurrentWeatherService
 */
@Injectable()
export class MockedCurrentWeatherService extends CurrentWeatherAbstract {
  /**
   * @ignore
   */
  constructor() {
    super();
  }

  /**
   * Getting the current weather
   * @param {ILocation} coordinates
   * @param {Function} errorCallback
   * @returns {Observable<any>}
   */
  getWeather(coordinates: ILocation, errorCallback: Function) {
    const position = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          position: 10
        });
      }, 1000);
    });

    const position2 = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          position: 2
        });
      }, 3000);
    });

    const weather = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          'coordinate': {
            'latitude': 56.29,
            'longitude': 84.58
          },
          'wind': {
            'speed': 52.36,
            'deg': 52.28
          },
          'main': {
            'temp': 16.49,
            'humidity': 84.93,
            'pressure': 80.81,
            'tempMin': 59.25,
            'tempMax': 44.91
          },
          'description': {
            'name': 'Малооблачно',
            'code': 2
          }
        });
      }, 5000);
    });

    return from([position, position2, weather])
      .pipe(mergeAll());
  }
}
