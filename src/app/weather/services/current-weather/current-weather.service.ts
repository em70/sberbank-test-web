import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { ILocation } from '../../../core/services/location/location';

import { CurrentWeatherAbstract } from './current-weather.abstract';
import { WeatherTransportAbstract } from '../weather-transport/weather-transport.abstract';

/**
 * Provides getting the current weather
 */
@Injectable()
export class CurrentWeatherService extends CurrentWeatherAbstract {

  constructor(private weatherTransport: WeatherTransportAbstract) {
    super();
  }

  /**
   * Getting the current weather
   * @param {ILocation} location
   * @param {Function} errorCallback
   * @returns {Observable<any>}
   */
  public getWeather(location: ILocation, errorCallback: (errorMessage: any) => void) {
    const
      privateChannel = '/user/weather/result',
      coordinatesChannel = '/app/weather/get',
      privateChannelSubscription = this.weatherTransport
        .subscribe(privateChannel);


    this.weatherTransport.errorSubject.subscribe(errorMessage => {
      if (errorCallback) {
        errorCallback(errorMessage);
      }
    });

    this.weatherTransport.publish(coordinatesChannel, JSON.stringify({
      latitude: location.latitude,
      longitude: location.longitude
    }));

    return privateChannelSubscription
      .pipe(map(message => {
        const body = JSON.parse(message.body);
        if (body.id) {
          console.log('---id---', body.id);
        }

        return body;
      }));
  }
}
